def funcion1(n):
    return n**2


def funcion2(n):
    return funcion1(n) + 1000*n


def funcion3(n):
    if n <= 0:
        return 10
    elif n > 1 and n < 5:
        return funcion1(n) - n + 1
    elif n >= 5:
        return 2*n - 1


def run():
    imprimir = True

    n = float(input('Ingrese un número decimal: '))

    print('''Ingrese:
             1, para seleccionar f1(n)
             2, para seleccionar f2(n)
             3, para seleccionar f3(n)
           ''')

    funcion = int(input('-----------> '))

    if funcion == 1:
        resultado = funcion1(n)
    elif funcion == 2:
        resultado = funcion2(n)
    elif funcion == 3:
        resultado = funcion3(n)
    else:
        imprimir = False
        print('\nSu elección no es válida\n')

    if imprimir == True:
        print(f'\nEl resultado de f{funcion}({n}) operación es: {resultado}\n')


if __name__ == '__main__':
    run()
