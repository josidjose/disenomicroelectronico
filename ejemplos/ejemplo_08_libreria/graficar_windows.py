import matplotlib.pyplot as plt
from milibreria import cuadrado


def run():
    a = []
    b = []

    for i in range(-20, 21):
        a.append(i)
        b.append(cuadrado(i))

    plt.plot(a, b)
    plt.show()


if __name__ == '__main__':
    run()
