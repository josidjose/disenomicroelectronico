#!/bin/bash

# * ------------------------------------------------------------------------------------
# * "THE BEER-WARE LICENSE" (Revision 42):
# * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
# * can do whatever you want with this stuff. If we meet some day, and you think
# * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
# * ------------------------------------------------------------------------------------

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo
echo "$VERDE ============= INICIO ============= $LIMPIAR"
echo

echo
echo "$LILA consultar: https://docs.aws.amazon.com/es_es/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html $LIMPIAR"
echo

echo
echo "$AZUL Actualizar $LIMPIAR"
echo
sudo yum update -y

echo
echo "$AZUL Instalar repositorios de lamp-mariadb10.2-php7.2 y php7.2 $LIMPIAR"
echo
sudo amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2

echo
echo "$AZUL Instalar PHP, MariaDB y Apache $LIMPIAR"
echo
sudo yum install -y httpd mariadb-server

echo
echo "$AZUL Iniciar y habilitar servidor web Apache $LIMPIAR"
echo
sudo systemctl start httpd
sudo systemctl enable httpd
sudo systemctl is-enabled httpd

echo
echo "$AZUL Establecer permisos de archivo $LIMPIAR"
echo
sudo usermod -a -G apache ec2-user

echo
echo "$LILA =============================================== $LIMPIAR"
echo "$LILA =     Cerrar sesión o cerrar la terminal      = $LIMPIAR"
echo "$LILA =============================================== $LIMPIAR"
echo

echo
echo "$VERDE Ejecute configurar_AMI_02.sh $LIMPIAR"
echo

exit 0
