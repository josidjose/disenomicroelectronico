def pares_for():

    pares = []

    for num in range(11):
        if num % 2 == 0:
            pares.append(num)

    print(pares)


def pares_list():
    pares = [num for num in range(11) if num % 2 == 0]

    print(pares)


def run():
    print('\nNúmeros pares de 0 a 10, obtenidos con un for: ')
    pares_for()

    print('\nNúmeros pares de 0 a 10, obtenidos con list comprenhension: ')
    pares_list()


if __name__ == '__main__':
    run()
